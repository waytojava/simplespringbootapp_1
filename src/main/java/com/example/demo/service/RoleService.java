package com.example.demo.service;

import com.example.demo.dto.RoleDTO;
import com.example.demo.entity.Role;
import com.example.demo.exception.RoleNotFoundException;
import com.example.demo.mappers.RoleMapper;
import com.example.demo.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Сервис для работы с ролями
 */
@Service
@Transactional
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    public RoleDTO getRoleById(Long roleId) {
        return roleRepository.findById(roleId).map(roleEntity -> roleMapper.toDTO(roleEntity)).orElseThrow(() -> new RoleNotFoundException());
    }

    public Long create(RoleDTO dto) {
        return roleRepository.save(roleMapper.toRole(dto)).getId();
    }

    public List<RoleDTO> readAll() {
        return roleMapper.toDTOS(roleRepository.findAll());
    }

    public void update(Long roleId, RoleDTO roleUpdated) {
        Optional<Role> optionalRole = roleRepository.findById(roleId);
        Role role = optionalRole.get();
        role.setRoleName(roleUpdated.getRoleName());
        roleRepository.save(role);
    }

    public void delete(Long id) {
        roleRepository.deleteById(id);
    }
}
