package com.example.demo.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.exception.*;
import com.example.demo.mappers.UserMapper;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Сервис для работы с пользователями
 */
@Service
@Transactional
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;

    public UserDTO getUserById(Long userId) {
        return userRepository.findById(userId).map(userEntity -> userMapper.toDTO(userEntity)).orElseThrow(() -> new UserNotFoundException());
    }

    public Long create(UserDTO dto) {
        return userRepository.save(userMapper.toUser(dto)).getId();
    }

    public List<UserDTO> readAll() {
        return userMapper.toDTOS(userRepository.getAllUsersWithRoles());
    }

    public void update(Long userId, UserDTO userUpdated) {
        Optional<User> optionalUser = userRepository.findById(userId);
        User user = optionalUser.get();
        user.setUsername(userUpdated.getUsername());
        user.setPassword(userUpdated.getPassword());
        user.setEmail(userUpdated.getEmail());
        userRepository.save(user);
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public void addRoleToUser(Long userId, List<Long> roleIDS) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException());
        for (Long roleId : roleIDS) {
            Role role = roleRepository.findById(roleId).orElseThrow(() -> new RoleNotFoundException());
            if (user.getRoles().contains(role)) {
                throw new RoleAlreadyAssignedException();
            }
            user.addRole(role);
            userRepository.save(user);
        }
    }

    public void deleteRoleToUser(Long userId, List<Long> roleIDS) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException());
        for (Long roleId : roleIDS) {
            Role role = roleRepository.findById(roleId).orElseThrow(() -> new RoleNotFoundException());
            if (!(user.getRoles().contains(role))) {
                throw new RoleAlreadyDeletedException();
            }
            user.deleteRole(role);
            userRepository.save(user);
        }
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException());
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList())
        );
    }
}
