package com.example.demo.service;

import com.example.demo.configs.JwtConfig;
import com.example.demo.constant.ClaimField;
import com.example.demo.entity.Jwt;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtService {
    private final JwtConfig jwtConfig;
    private final UserRepository userRepository;

    private SecretKey key;

    private SecretKey generatedSecretKey() {
        if (key == null) {
            key = Keys.hmacShaKeyFor(jwtConfig.getSigningKey().getBytes(StandardCharsets.UTF_8));
        }
        return key;
    }

    public String generatedJwt(Authentication authentication) {
        return Jwts.builder()
                .setClaims(
                        Map.of(
                                ClaimField.USERNAME, authentication.getName(),
                                ClaimField.ROLE, authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()),
                                ClaimField.USER_ID, String.valueOf(userRepository.findByUsername(authentication.getName()).get().getId())))
                .setExpiration(new Date(new Date().getTime() + jwtConfig.getExpiration()))
                .setSubject(authentication.getName())
                .signWith(generatedSecretKey())
                .compact();
    }

    public Claims getClaims(String jwt) {
        return Jwts.parserBuilder()
                .setSigningKey(generatedSecretKey())
                .build()
                .parseClaimsJws(jwt)
                .getBody();
    }

    public boolean isValidJwt(Jwt jwt) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(generatedSecretKey())
                .build()
                .parseClaimsJws(jwt.getToken())
                .getBody();

        Optional<User> user = userRepository.findByUsername(String.valueOf(claims.get(ClaimField.USERNAME)));

        return claims.getExpiration().after(new Date()) && user.isPresent();
    }
}