package com.example.demo.constant;

public final class HeaderValues {
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";

    private HeaderValues() {
    }
}