package com.example.demo.constant;

public final class ClaimField {
    public static final String USERNAME = "username";
    public static final String ROLE = "role";
    public static final String USER_ID = "user_id";

    private ClaimField() {
    }
}