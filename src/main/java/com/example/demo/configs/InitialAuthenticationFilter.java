package com.example.demo.configs;

import com.example.demo.dto.UserDTO;
import com.example.demo.constant.HeaderValues;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.demo.service.UsernamePasswordAuthentication;
import com.example.demo.service.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class InitialAuthenticationFilter extends OncePerRequestFilter {
    private final JwtService jwtService;
    private final UsernamePasswordAuthenticationProvider authenticationProvider;

    private String authorization = "Authorization";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        if (request.getHeader(authorization) == null) {
            String bodyJson = request.getReader().readLine();
            if (bodyJson != null) {
                ObjectMapper mapper = new ObjectMapper();
                UserDTO userDto = mapper.readValue(bodyJson, UserDTO.class);
                String username = userDto.getUsername();
                String password = userDto.getPassword();
                try {
                    Authentication authentication = new UsernamePasswordAuthentication(username, password);
                    authentication = authenticationProvider.authenticate(authentication);
                    String jwt = jwtService.generatedJwt(authentication);
                    response.setHeader(authorization, HeaderValues.BEARER + jwt);
                } catch (BadCredentialsException | ObjectNotFoundException e) {
                    logger.error(e.getMessage());
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
            }
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return !request.getServletPath().equals("/login");
    }
}
