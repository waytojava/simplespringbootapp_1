package com.example.demo.dto;

import lombok.Data;

/**
 * Это DTO роли, для общения между клиентом и сервисом
 */
@Data
public class RoleDTO {
    private String roleName;
}
