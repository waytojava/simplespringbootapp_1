package com.example.demo.dto;

import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.util.List;

/**
 * Это DTO пользователя, для общения между клиентом и сервисом
 */
@Data
public class UserDTO {
    private String username;
    private String password;
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$")
    private String email;
    private List<RoleDTO> roles;
}
