package com.example.demo.contorllers;

import com.example.demo.dto.RoleDTO;
import com.example.demo.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Контроллер работы с ролями
 */
@RestController
@AllArgsConstructor
@RequestMapping("api/v1/roles")
public class RoleController {
    private final RoleService roleService;

    @PostMapping()
    public Long create(@RequestBody RoleDTO dto) {
        return roleService.create(dto);
    }

    @GetMapping()
    public List<RoleDTO> readAll() {
        return roleService.readAll();
    }

    @GetMapping(value = "/{id}")
    public RoleDTO getOneRole(@PathVariable Long id) {
        return roleService.getRoleById(id);
    }

    @PatchMapping("/{id}")
    public void update(@PathVariable("id") Long roleId,
                       @RequestBody RoleDTO roleUpdated) {
        roleService.update(roleId, roleUpdated);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        roleService.delete(id);
    }
}
