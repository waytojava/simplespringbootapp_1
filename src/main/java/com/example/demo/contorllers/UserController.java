package com.example.demo.contorllers;

import com.example.demo.dto.UserDTO;
import com.example.demo.service.KafkaProducer;
import com.example.demo.service.UserService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Контроллер работы с пользователями
 */
@RestController
@AllArgsConstructor
@RequestMapping("api/v1/users")
public class UserController {
    private final UserService userService;
    private final KafkaProducer kafkaProducer;

    @PostMapping()
    public Long create(@Valid @RequestBody UserDTO dto) {
        return userService.create(dto);
    }

    @GetMapping()
    public List<UserDTO> readAll() {
        return userService.readAll();
    }

    @GetMapping(value = "/{id}")
    public UserDTO getOneUser(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @PatchMapping("/{id}")
    public void update(@PathVariable("id") Long userId,
                       @Valid @RequestBody UserDTO userUpdated) {
        userService.update(userId, userUpdated);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        userService.delete(id);
    }

    @PostMapping("/addRoleTo/{userId}")
    public void addRole(@RequestBody final List<Long> roleIDS, @PathVariable Long userId) {
        userService.addRoleToUser(userId, roleIDS);
    }

    @PostMapping("/delRoleTo/{userId}")
    public void deleteRole(@RequestBody final List<Long> roleIDS, @PathVariable Long userId) {
        userService.deleteRoleToUser(userId, roleIDS);
    }

    @PostMapping("/kafka/send")
    public void sendMessage(@RequestBody String message) {
        kafkaProducer.sendMessage(message);
    }
}
