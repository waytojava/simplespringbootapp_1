package com.example.demo.contorllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/say")
public class HelloWorldController {
    @GetMapping(value = "/hello")
    public String sayHello() {
        return "Hello World";
    }
}