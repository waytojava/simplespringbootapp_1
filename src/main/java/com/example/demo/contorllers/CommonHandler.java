package com.example.demo.contorllers;

import com.example.demo.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;

/**
 * Здесь перехватываются все кастомные ошибки
 */
@RestControllerAdvice
public class CommonHandler {
    @ExceptionHandler(value = {UserNotFoundException.class})
    public ResponseEntity<ExceptionMessage> handleException(UserNotFoundException e) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder().errorCode(400).message(DemoExceptionKey.USER_NOT_FOUND.getKey()).date(new Date()).build();
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {RoleNotFoundException.class})
    public ResponseEntity<ExceptionMessage> handleException(RoleNotFoundException e) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder().errorCode(400).message(DemoExceptionKey.ROLE_NOT_FOUND.getKey()).date(new Date()).build();
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {RoleAlreadyAssignedException.class})
    public ResponseEntity<ExceptionMessage> handleException(RoleAlreadyAssignedException e) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder().errorCode(409).message(DemoExceptionKey.ROLE_ALREADY_ASSIGNED.getKey()).date(new Date()).build();
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {RoleAlreadyDeletedException.class})
    public ResponseEntity<ExceptionMessage> handleException(RoleAlreadyDeletedException e) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder().errorCode(409).message(DemoExceptionKey.ROLE_ALREADY_DELETED.getKey()).date(new Date()).build();
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionMessage> handleException(MethodArgumentNotValidException e) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder().errorCode(400).message(DemoExceptionKey.INVALID_USER_EMAIL.getKey()).date(new Date()).build();
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {BadCredentialsException.class})
    public ResponseEntity<ExceptionMessage> handleException(BadCredentialsException e) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder().errorCode(400).message(DemoExceptionKey.BAD_CREDENTIALS.getKey()).date(new Date()).build();
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }
}
