package com.example.demo.mappers;

import com.example.demo.dto.RoleDTO;
import com.example.demo.entity.Role;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class RoleMapper {
    public abstract RoleDTO toDTO(Role role);

    public abstract Role toRole(RoleDTO dto);

    public abstract List<Role> toUsers(List<RoleDTO> dtos);

    public abstract List<RoleDTO> toDTOS(List<Role> roles);
}
