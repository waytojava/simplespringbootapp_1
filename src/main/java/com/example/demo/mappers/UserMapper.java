package com.example.demo.mappers;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
    public abstract UserDTO toDTO(User user);

    public abstract User toUser(UserDTO dto);

    public abstract List<User> toUsers(List<UserDTO> dtos);

    public abstract List<UserDTO> toDTOS(List<User> users);
}
