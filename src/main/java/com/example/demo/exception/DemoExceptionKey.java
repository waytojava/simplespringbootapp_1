package com.example.demo.exception;

public enum DemoExceptionKey {
    USER_NOT_FOUND,
    ROLE_NOT_FOUND,
    ROLE_ALREADY_ASSIGNED,
    ROLE_ALREADY_DELETED,
    INVALID_USER_EMAIL,
    BAD_CREDENTIALS,
;

    public String getKey() {
        return this.name();
    }
}