package com.example.demo.exception;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ExceptionMessage {
    private int errorCode;
    private String message;
    private Date date;
}
